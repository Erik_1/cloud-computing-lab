FROM ruby:3.1.2
RUN apt-get update && apt-get install -y postgresql-client
WORKDIR /lab
COPY Gemfile /lab/Gemfile
COPY Gemfile.lock /lab/Gemfile.lock
RUN gem update bundler && bundle install

EXPOSE 3000

# Configure the main process to run when running the image
# I don't need this, docker compose already runs it (overwriting this one)
#CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
