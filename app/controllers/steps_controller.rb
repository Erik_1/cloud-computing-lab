class StepsController < ApplicationController
  def index
    @steps = Step.all
  end

  def show
    @step = Step.find(params[:id])
  end

  def new # show form
    @step = Step.new
  end

  def create # take data submitted by form, make thing, includes validation
    @step = Step.new(step_params)

    if @step.save
      redirect_to @step
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit # show form
    @step = Step.find(params[:id])
  end

  def update # take data submitted by form, update thing, includes validation
    @step = Step.find(params[:id])

    if @step.update(step_params)
      redirect_to @step
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @step = Step.find(params[:id])
    @step.destroy

    redirect_to root_path, status: :see_other
  end

  private
    def step_params
      params.require(:step).permit(:name, :description)
    end
end
